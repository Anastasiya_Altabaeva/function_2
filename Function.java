import java.io.*;

public class Function {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\data"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\sum.txt"))) {
            String string = bufferedReader.readLine();
            while (string != null) {
                String[] strings = string.split(" ");
                int time = Integer.parseInt(strings[0]);
                int money[] = new int[strings.length];
                for (int i = 1; i < strings.length; i++) {
                    money[i] = Integer.parseInt(strings[i]);
                }
                int a;
                for (int i = 0; i < money.length; i++) {
                    for (int j = 0; j < money.length - 1; j++) {
                        if (money[j] < money[j + 1]) {
                            a = money[j];
                            money[j] = money[j + 1];
                            money[j + 1] = a;
                        }
                    }
                }
                if (money.length < time) {
                    time = money.length;
                }
                int sum = 0;

                for (int i = 0; i < time; i++) {
                    sum = sum + money[i];
                }
                bufferedWriter.write(String.valueOf(sum) + "\n");
                string = bufferedReader.readLine();


            }
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}






